require 'eth'

client = Eth::Client.create("https://polygon-rpc.com/")
abi = '[{
    "inputs": [],
    "name": "totalSupply",
    "outputs": [
        {
            "internalType": "uint256",
            "name": "",
            "type": "uint256"
        }
    ],
    "stateMutability": "view",
    "type": "function"
}]'
contract_address = "0xA1c57f48F0Deb89f569dFbE6E2B7f46D33606fD4"
contract_name= "UChildERC20Proxy"

contract = Eth::Contract.from_abi(name: contract_name, address: contract_address, abi: abi)

puts "Total supply: #{client.call(contract, "totalSupply")}"