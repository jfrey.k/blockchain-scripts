require "uri"
require "json"
require "net/http"
require "eth"

url = URI("https://polygon-rpc.com/")

https = Net::HTTP.new(url.host, url.port)
https.use_ssl = true

request = Net::HTTP::Post.new(url)
request["Content-Type"] = "application/json"

request.body = JSON.dump({
  "method": "eth_getLogs",
  "params": [
    {
      "blockHash": "0x8f5210b3052133904b5596a1345dc361e832eed56b181226c459eecb51113336",
      "topics": [
        "0xd78ad95fa46c994b6551d0da85fc275fe613ce37657fb8d5e3d130840159d822"
      ]
    }
  ],
  "id": 1,
  "jsonrpc": "2.0"
})

response = https.request(request)
data = JSON.parse(response.body)

puts data

# Simplified output
# data["result"].each do |event|
#   # blockNumber = event["blockNumber"].to_i(0)
#   transactionHash =  event["transactionHash"]

#   amount0In, amount1In, amount0Out, amount1Out = Eth::Abi.decode(["uint256","uint256","uint256","uint256"], event["data"])

#   event = {
#     # "blockNumber" => blockNumber,
#     "transactionHash" => transactionHash,
#     "amount0In" => amount0In,
#     "amount1In" => amount1In,
#     "amount0Out" => amount0Out,
#     "amount1Out" => amount1Out
#   }

#   puts event
# end
