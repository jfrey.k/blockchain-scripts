require 'optparse'

Options = Struct.new(:tk_a_pool_balance,:tk_b_pool_balance,:tk_b_market_price,:tk_a_exchange_amt)

# ruby get_price_impact.rb --tk_a_pool_balance=2000000 --tk_b_pool_balance=1000 --tk_b_market_price=2000 --tk_a_exchange_amt=10000

# ruby get_price_impact.rb --tk_a_pool_balance=200000000 --tk_b_pool_balance=70000 --tk_a_exchange_amt=5000 --tk_b_market_price=3500 

# ruby get_price_impact.rb --tk_a_pool_balance=200000000 --tk_b_pool_balance=57000 --tk_a_exchange_amt=5000 --tk_b_market_price=3500

# ruby get_price_impact.rb --tk_a_pool_balance=3024 --tk_b_pool_balance=4318 --tk_a_exchange_amt=100 --tk_b_market_price=0.700322422

# ruby get_price_impact.rb --tk_a_pool_balance=3024 --tk_b_pool_balance=4318 --tk_a_exchange_amt=1000 --tk_b_market_price=0.700322422

class Parser
  def self.parse(options)
    args = Options.new("world")

    opt_parser = OptionParser.new do |opts|
      opts.banner = "Usage: example.rb [options]"

      opts.on("--tk_a_pool_balance=TOKEN_A_POOL_BALANCE", "Token A Pool Balance") do |n|
        args.tk_a_pool_balance = n.to_f
      end

      opts.on("--tk_b_pool_balance=TOKEN_B_POOL_BALANCE", "Token B Pool Balance") do |n|
        args.tk_b_pool_balance = n.to_f
      end

      opts.on("--tk_b_market_price=TOKEN_B_MARKET_PRICE", "Market Price of Token B per Token A") do |n|
        args.tk_b_market_price = n.to_f
      end

      opts.on("--tk_a_exchange_amt=TOKEN_A_EXCHANGE_AMOUNT", "Exchange Token A amount for Token B ") do |n|
        args.tk_a_exchange_amt = n.to_f
      end

      opts.on("-h", "--help", "Prints this help") do
        puts opts
        exit
      end
    end

    opt_parser.parse!(options)
    return args
  end
end
options = Parser.parse(ARGV)
puts options

constant_product = options.tk_a_pool_balance * options.tk_b_pool_balance

# tk_a_exchange_amt_with_fee = options.tk_a_exchange_amt * 0.997

new_tk_a_pool_balance = options.tk_a_pool_balance + options.tk_a_exchange_amt

new_tk_b_pool_balance = (constant_product / new_tk_a_pool_balance)

received_tk_b_amt = (options.tk_b_pool_balance - new_tk_b_pool_balance)

price_per_paid_tk_b = (options.tk_a_exchange_amt / received_tk_b_amt)



puts "Constant Product: #{constant_product}"
# puts "Token A Exchange Amount With Fee: #{tk_a_exchange_amt_with_fee}"
puts "New Token A Pool Balance: #{new_tk_a_pool_balance}"
puts "New Token B Pool Balance: #{new_tk_b_pool_balance}"
puts "Received Token B Amount: #{received_tk_b_amt}"
puts "Price Per Paid Token B: #{price_per_paid_tk_b}"


if options.tk_b_market_price != nil
    price_impact = ((1 - (options.tk_b_market_price / price_per_paid_tk_b )) * 100).round(2)
    puts "Price impact: #{price_impact}%"
end


# Sources:
# https://medium.com/@crjameson/calculating-the-optimal-token-amounts-before-swapping-on-uniswap-v2-and-uniswap-v3-in-python-acf5f5c6d47e

# https://jeiwan.net/posts/programming-defi-uniswapv2-4/

# https://stackoverflow.com/questions/74282316/uniswap-v2-pool-priceimpact#:~:text=You%20can%20calculate%20the%20price,is%20ETH%2C%20k%20is%202%2C000%2C000%2C000.

# https://dailydefi.org/articles/price-impact-and-how-to-calculate/

# https://medium.com/@tomarpari90/constant-product-automated-market-maker-everything-you-need-to-know-5bfeb0251ef2#:~:text=The%20constant%20product%20formula%20is,product%20must%20still%20equal%20k%20.

# https://medium.com/luchadores-chronicles/how-to-calculate-price-impact-b4c87d8b10ed

