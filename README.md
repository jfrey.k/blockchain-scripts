## Contract Calls Knowledge

1. Using [https://polygon-rpc.com/](https://polygon-rpc.com/) RPC node as a service, write the code and RPC call to obtain `totalSupply` of the [MANA token](https://polygonscan.com/token/0xa1c57f48f0deb89f569dfbe6e2b7f46d33606fd4) issued on the Polygon (MATIC) blockchain. You may consider using the ERC-20 ABI for your solution. 

** _You may approach the above natively or with a library such as [Ethereum.rb](https://github.com/EthWorks/ethereum.rb), [web3js](https://web3js.readthedocs.io/en/v1.7.1/), or [ethers](https://docs.ethers.io/v5/)_
    
> The following script should outputs the total supply of the [MANA token](https://polygonscan.com/token/0xa1c57f48f0deb89f569dfbe6e2b7f46d33606fd4) issued on the Polygon (MATIC) blockchain. 
> ```sh
> $ ruby get_total_supply.rb
> Total supply: 4988473887434810290802055
> ```


## DEX event logs

1.  Using the [Etherscan](https://etherscan.io/) block explorer, find a list of recent swaps for the following [USDC/ETH pool on Uniswap V2](https://v2.info.uniswap.org/pair/0xb4e16d0168e52d35cacd2c6185b44281ec28c9dc)? Provide a screenshot for your response.

> <img src="https://gitlab.com/jfrey.k/blockchain-scripts/-/raw/main/screenshots/dex-q1.png?ref_type=heads" width="400">


<br />
2.  [https://etherscan.io/tx/0x5e555836bacad83ac3989dc1ec9600800c7796d19d706f007844dfc45e9703ac/](https://etherscan.io/tx/0x5e555836bacad83ac3989dc1ec9600800c7796d19d706f007844dfc45e9703ac/) is a swap transaction on a Uniswap V2 pool. One of the associated swaps here is a trade from 1.15481 ETH to $3,184.35. Determine in the block explorer where that raw number is coming from and how it is being derived. (You may use screenshot to show your answers)

> <img src="https://gitlab.com/jfrey.k/blockchain-scripts/-/raw/main/screenshots/dex-q2-1.png?ref_type=heads" width="400">
> <img src="https://gitlab.com/jfrey.k/blockchain-scripts/-/raw/main/screenshots/dex-q2-2.png?ref_type=heads" width="400">
>
>
> The transfer event in the first screenshot shows us where the 1.15481 ETH raw number is coming from. The src address is the DOMI-ETH pair followed by the USDC-ETH pair as the dst address, which means 1.15481 ETH was allocated to USDC-ETH from DOMI-ETH, as a result the ETH of DOMI-ETH pool is decreased, conversely increases the ETH on USDC-ETH.
>
> <img src="https://gitlab.com/jfrey.k/blockchain-scripts/-/raw/main/screenshots/dex-q2-3.png?ref_type=heads" width="400">
> <img src="https://gitlab.com/jfrey.k/blockchain-scripts/-/raw/main/screenshots/dex-q2-4.png?ref_type=heads" width="400">
>
> In summary, this basically tells us that after the user has agreed to trade 25000 DOMI to $3184.35. An event was triggered to transfer the 25000 DOMI from user account to Uniswap. Because DOMI-USDC pool wasn’t even available at all, that amount will first go into the DOMI-ETH pool and swap from 25000 DOMI to 1.15481 ETH then the ETH amount will be transferred to USDC-ETH pool and swap from 1.15481 ETH to $3184.35.

 <br />
3.  Quickswap, a DEX on Polygon (MATIC) allows users to swap two assets as a trade. For every swap transaction that is recorded on the blockchain, a swap event is emitted and stored in the network with this hash ID `0xd78ad95fa46c994b6551d0da85fc275fe613ce37657fb8d5e3d130840159d822`. Write the RPC API call to get all the swap events that were emitted for the block [#26444465](https://polygonscan.com/block/26444465). Use [https://polygon-rpc.com/](https://polygon-rpc.com/) RPC node as a service.

> The following script should outputs all the swap events that were emitted for the block [#26444465](https://polygonscan.com/block/26444465) 
> ```sh
> $ ruby get_list_of_swap_events.rb
> {"jsonrpc"=>"2.0", "id"=>1, "result"=>[{"address"=>"0xadbf1854e5883eb8aa7baf50705338739e558e5b", "topics"=>["0xd78ad95fa46c994b6551d0da85fc275fe613ce37657fb8d5e3d130840159d822", "0x000000000000000000000000a5e0829caced8ffdd4de3c43696c57f7d7a678ff", "0x000000000000000000000000a5e0829caced8ffdd4de3c43696c57f7d7a678ff"], "data"=>"0x0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000ff158ca43224800000000000000000000000000000000000000000000000078ebde1bf800453c0000000000000000000000000000000000000000000000000000000000000000", "blockNumber"=>"0x19382b1", "transactionHash"=>"0x3483fd0fa5d38905e28245ca9ca87b0ab331a104c509e3d239be8f1e5337c01b", "transactionIndex"=>"0x1d", "blockHash"=>"0x8f5210b3052133904b5596a1345dc361e832eed56b181226c459eecb51113336", "logIndex"=>"0x5b", "removed"=>false}]}
> ```

 <br />
4.  When using the Quickswap DEX, we noticed that the price impact is -42.09% when we increase the size of the trade. What does price impact mean, why is it important, the math behind the price impact. Include as many details as you can to support your explanation.

> Price impact is the effect of a trade that affects the price of an asset. It’s important because it shows useful insight to the users on how much difference in value between two assets when making a trade. The difference can be drastic when user makes large trade offer on pools with low liquidity causes high impact to the price of an asset which is in this case. On the contrary, pools with high liquidity may have smaller impact and it all depends on the size of the trade and liquidity pools.To understand the math behind the price impact, we need to first understand the formula of how most DEXs maintain the balance of a liquidity pool. A liquidity pool comprises two token pools and the multiplication of the two token pools is what we known as Constant Product, hence the equation is x * y = k. The x is the pool balance of token A, y is the pool balance of token B, and k is the Constant Product.
> 
> ```sh
> Pool balance of Token A * Pool balance of Token B  = Constant Product
> ``` 
>
> For instance, given that a liquidity pool that has 200000000 UDSC and 57000 ETH pools, and a user exchange 5000 UDCS for ETH. Let x = 200000000 UDSC and y = 57000 ETH, then the Constant Product of the liquidity pool is 11400000000000. For simplicity, let’s assume there’s no liquidity provider fees and add the paid token which is 5000 UDCS to the USDC pool which gives us 200005000, the new pool balance of UDSC. This means with incremental in UDSC amount leads to decremental in ETH amount. To find the ETH amount that sells to the user, we need to get the new pool balance of ETH by transposing the x in x * y  = k formula and that gives us y = k / x.
> 
> ```sh
> New pool balance of Token B = Constant Product / New pool balance of Token A 
> ``` 
> 
> By using the formula above, we can substitute both the Constant Product and the new pool balance of USDC, and that gives us 56998.57503562411 as the new pool balance of ETH. Now we have both the old and new pool balance of ETH, subtracting the new pool balance would give us 1.4249643758885213 ETH, which is the amount that user will receive. 
> 
> Now we have all the variables that we need to calculate the price impact by using the formula below. Let’s assume the market price for 1 ETH = 3500 USDC, and the price per paid ETH is 3508.8596491279322 USDC (5000 USDC/1.4249643758885213 ETH). Substituting the values to the formula and we get 0.25% as the price impact.
> 
> ```sh
> Price impact  = (1 - (Market Price / Price per paid ETH)) * 100
> ``` 
> 
> It’s worth mentioning that the formula here is largely based on Uniswap V2 and may not be as accurate as the DEXs out there as I believe each of the DEXs has different pricing model. I’ve seen example that tries to use ABI to call the methods to perform the calculations and get the possible closest results.
>
>
> Summary of result
>```sh
> Pool balance of UDSC: 200000000 UDSC
> Pool balance of ETH: 57000 ETH
> Market Price: 1 ETH -> 3500 USDC
>
> Exchange 5000 UDSC for ETH
>
> Total liquidity pool: 11400000000000
> New pool balance of UDSC: 200005000 UDSC
> New pool balance of ETH: 1.4249643758885213 ETH
> Price per paid ETH: 1 ETH -> 3508.8596491279322
> Price impact: 0.25%
> ``` 
>
